terraform {
  backend "gcs" {
    bucket  = "tf-glci-kfinf-smp"
    prefix  = "terraform/state/prod"
    credentials  = "/tmp/auth.json"
    
  }
}
