# gcp-sample

Sample deployment on GCP using Kubernetes and Terraform

## Description
This pipeline will create a k8s cluster and network in GCP, and then deploy loadbalancer, application and ingress on the cluster.
There are 6 build stages:
* plan - terraform creates the execution plan
* apply - terraform will apply the changes determined on the execution plan
* lint - basic source code check using npm
* build - kaniko will build the sample application image and stage it to docker registry
* deploy - apply loadbalancer, application and ingress to the cluster __\*this step has to be triggered manually\*__
* destroy - destroy the Kubernetes Cluster __\*this step has to be triggered manually\*__

## Pre-requisites
The pipeline expects the environment to be previously configured as follows (all variables must be set on GitLabCI environment)

### Variables
1. __GitLab Variables__
    * __CI_REGISTRY__ - Docker Registry which the pipeline will pull CI image from
	* __CIDR_RANGE__ - IP range to be provisioned
2. __GCP Variables__
	* __NODE_NAME__ - Cluster node name
    * __PROJECT__ - GCP project ID
	* __REGION__ - GCP region where resources will be created
	* __SERVICE_ACC__ - GCP service account (with privileges to manage resources) __\*SENSITIVE CONTENT\*__
	* __SUBNET_NAME__ - Name for subnetwork
	* __VPC_NAME__ - Name for private network
	* __ZONE__ - Cluster zone
3. __Docker Registry Variables__
	* __REGISTRY_LOGIN__ - Docker registry username (with RW access to repository) __\*SENSITIVE CONTENT\*__
	* __REGISTRY_PASSWORD__ - ${REGISTRY_LOGIN}'s password __\*SENSITIVE CONTENT\*__
	* __REPOSITORY__ - Repository where generated image will be pushed to

### GCP Environment
1. Project
    * Project must exist, with the same ID set in variable
2. Bucket
	* Storage bucket must exist, with the same ID set in __backend.tf__
3. Service Account
	* Service account must exist (with administrative privileges on needed resources), and .json format key set in environment variable

## How it works

### Terraform
Terraform modules used to provision the cluster:
- network (vpc, subnet, firewall)
- cluster

### Application
- CI/CD calls are made from the application folder (directly by Kubernetes)
- Once all is provisioned, need to click on the generated LoadBalancer IP (http)
- LoadBalancer (directly by Kubernetes)

### Docker
- Main Dockerfile to build the image used within GitLabCI.
- Dockerfile insise source code to build the final image.
- Kaniko used to run docker inside docker (https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)

### General Notes
- Deployments are splitted by namespaces (prod/staging).
- Kubernetes was used directly without Helm Chart.

## WorkFlow
1. Stage "plan" is automatically triggered when there is a commit on the repo.
2. Stage "apply" is automatically triggered when "plan" completes w/o errors.
3. Stage "lint" is automatically triggered when "apply" completes w/o errors.
4. Stage "build" is automatically triggered when "lint" completes w/o errors.
5. After "build" is completed, "deploy" can be triggered manually.
6. Stage "destroy" can be triggered at any moment AFTER "build" is completed.
